#include "Arduino.h"
#include "SevenSegDisp.h"
#include <DS18B20.h>

#define DISPLAY_TIMER (uint8_t) 2
#define DIGIT_PIN_1 (uint8_t) 10
#define DIGIT_PIN_A (uint8_t) 3
#define MINUS_PIN (uint8_t) 12
#define PUBLIC_CATHODE (bool) true
#define PUBLIC_PIN_TRANSISTOR (bool) true
#define LIGHT_SENSOR_PIN (uint8_t) A0
#define TEMP_SENSOR_PIN 2

SevenSegDisp disp(DIGIT_PIN_A, DIGIT_PIN_1, MINUS_PIN, LIGHT_SENSOR_PIN);
DS18B20 ds(TEMP_SENSOR_PIN);

float temp = 99;
float tempOld = 99;

void setup() {
    disp.displayInit(DISPLAY_TIMER, PUBLIC_PIN_TRANSISTOR, PUBLIC_CATHODE);
    ds.setResolution(9);
}

void loop() {
    ds.resetSearch();

    if (ds.selectNext()) {
        temp = ds.getTempC();
        if (temp != tempOld)
            disp.write((int16_t) temp);
    } else {
        temp = -99;
        disp.write((int16_t) temp);
    }

    delay(500);

}

ISR(TIMER2_OVF_vect) {
    disp.interruptHandler();
}

ISR(TIMER2_COMPA_vect) {
    disp.brightnessHandler();
}
