/*
 * segments display driver
 */

#ifndef SEVENSEGDISP_H
#define SEVENSEGDISP_H

#include "stdint.h"

#define SEG_A (uint8_t) 0x01
#define SEG_B (uint8_t) 0x02
#define SEG_C (uint8_t) 0x04
#define SEG_D (uint8_t) 0x08
#define SEG_E (uint8_t) 0x10
#define SEG_F (uint8_t) 0x20
#define SEG_G (uint8_t) 0x40

#define DIG_0 (uint8_t) (SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F)
#define DIG_1 (uint8_t) (SEG_B | SEG_C)
#define DIG_2 (uint8_t) (SEG_A | SEG_B | SEG_D | SEG_E | SEG_G)
#define DIG_3 (uint8_t) (SEG_A | SEG_B | SEG_C | SEG_D | SEG_G)
#define DIG_4 (uint8_t) (SEG_B | SEG_C | SEG_F | SEG_G)
#define DIG_5 (uint8_t) (SEG_A | SEG_C | SEG_D | SEG_F | SEG_G)
#define DIG_6 (uint8_t) (SEG_A | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G)
#define DIG_7 (uint8_t) (SEG_A | SEG_B | SEG_C)
#define DIG_8 (uint8_t) (SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G)
#define DIG_9 (uint8_t) (SEG_A | SEG_B | SEG_C | SEG_D | SEG_F | SEG_G)
#define CHAR_MINUS (uint8_t) (SEG_G)
#define MINUS (uint8_t) 10
#define DIG_COUNT (uint8_t) 2
#define BRIGHT_DELAY 20
#define BRIGHTNESS_MIN 15

class SevenSegDisp {
public:
    // Constructor
    SevenSegDisp(uint8_t, uint8_t, uint8_t, uint8_t);

    void displayInit(uint8_t, bool, bool);
    void interruptHandler(void);
    void brightnessHandler(void);
    void write(int16_t value);

private:
    void startTimer(uint8_t);
    void adjBrightness(void);

    uint8_t _SegLevel;
    uint8_t _DigLevel;
    uint8_t _digPin1;
    uint8_t _currentDigPin;
    uint8_t _minusPinNum;
    uint8_t _lightSensorPin;
    int16_t _dispValue;
    uint8_t _timerID;
    uint8_t _segPinA;
    uint8_t _brightness;
    uint8_t _brightnessOld;
    uint8_t _brightnessDelay;
};

#endif
