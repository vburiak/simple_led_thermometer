/*
 * 7 segments display driver
 */

#include "Arduino.h"
#include "SevenSegDisp.h"

const uint8_t segments[] = { SEG_A, SEG_B, SEG_C, SEG_D, SEG_E, SEG_F, SEG_G };
const uint8_t digits[] = { DIG_0, DIG_1, DIG_2, DIG_3, DIG_4, DIG_5, DIG_6,
DIG_7, DIG_8, DIG_9, CHAR_MINUS };

uint8_t bright = 0;

SevenSegDisp::SevenSegDisp(uint8_t segPinA, uint8_t digPin1, uint8_t minusPinNum, uint8_t lightSensorPin) {

    _minusPinNum = minusPinNum;
    _lightSensorPin = lightSensorPin;
    _SegLevel = HIGH;
    _DigLevel = LOW;
    _digPin1 = digPin1;
    _segPinA = segPinA;
    _currentDigPin = 0;
    _dispValue = 0;
    _timerID = 0;
    _brightness = 100 - (analogRead(_lightSensorPin) / 10);
    _brightnessOld = _brightness;
    _brightnessDelay = 0;
}

void SevenSegDisp::startTimer(uint8_t timerID) {
    //stop interrupts
    cli();

    _timerID = timerID;
    if (_timerID == 0) {
        TCCR0A = 0;  // set entire TCCR2A register to 0
        TCCR0B = 0;  // same for TCCR2B
        TCNT0 = 0;  // initialize counter value to 0
        OCR0A = 255;  // 16/256/256 = 122Hz
        TCCR0A = 0;  // normal mode
        TCCR0B |= (1 << CS02);  // Set CS02 bits for 256 prescaler
        TIMSK0 |= (1 << OCIE0A);  // enable timer compare interrupt
    } else if (_timerID == 1) {
        TCCR1A = 0;  // set entire TCCR1A register to 0
        TCCR1B = 0;  // same for TCCR1B
        TCNT1 = 0;  //initialize counter value to 0
        OCR1A = 255;  // 16/1024/155 = 50Hz
        TCCR1B = 0;  // normal mode
        TCCR1B |= (1 << CS12);  // Set CS12 bits for 256 prescaler
        TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt
    } else if (_timerID == 2) {
        TCCR2A = 0;  // set entire TCCR2A register to 0
        TCCR2B = 0;  // same for TCCR2B
        TCNT2 = 0;  //initialize counter value to 0
        OCR2A = 255;  // 16/1024/155 = 50Hz
        TCCR2A = 0;  // normal mode
        TCCR2B |= (1 << CS22) | (1 << CS21); // Set CS22 and CS21 bit for 256 prescaler
        TIMSK2 |= (1 << OCIE2A) | (1 << TOIE2); // enable timer compare interrupt
    }

    //allow interrupts
    sei();

}

void SevenSegDisp::displayInit(uint8_t timerID, bool withTransistor, bool publicCathode) {

    if (publicCathode) {
        _DigLevel = LOW;
        _SegLevel = HIGH;
    } else {
        _DigLevel = HIGH;
        _SegLevel = LOW;
    }

    if (withTransistor) {
        _DigLevel = !_DigLevel;
    }

    digitalWrite(_minusPinNum, _SegLevel);
    pinMode(_minusPinNum, OUTPUT);

    for (uint8_t i = 0; i < sizeof(segments); i++) {
        digitalWrite(_segPinA + i, !_SegLevel);
        pinMode(_segPinA + i, OUTPUT);
    }

    for (uint8_t i = 0; i < DIG_COUNT ; i++) {
        digitalWrite(_digPin1 + i, !_DigLevel);
        pinMode(_digPin1 + i, OUTPUT);
    }

    startTimer(timerID);
}

void SevenSegDisp::interruptHandler(void) {
    int16_t divider = 1;
    int16_t character;

    // switch off digit
    digitalWrite(_digPin1 + _currentDigPin, !_DigLevel);

    adjBrightness();

    if ((++_currentDigPin) >= DIG_COUNT)
        _currentDigPin = 0;

    for (uint8_t i = 0; i < _currentDigPin; i++) {
        divider = divider * 10;
    }

    if (_dispValue < 0) {
        if (_dispValue > -10 && _currentDigPin > 0) {
            character = MINUS;
        } else {
            character = _dispValue * -1 / divider % 10;
        }
    } else {
        character = _dispValue / divider % 10;
    }

    for (byte i = 0; i < sizeof(segments); i++) {
        if (digits[character] & 0x01 << i) {
            digitalWrite(_segPinA + i, _SegLevel);
        } else {
            digitalWrite(_segPinA + i, !_SegLevel);
        }
    }

    if (_dispValue < -9)
        digitalWrite(_minusPinNum, _SegLevel);
    else
        digitalWrite(_minusPinNum, !_SegLevel);

    // switch on digit
    if (!((_currentDigPin > 0) && (character == 0))) {
        digitalWrite(_digPin1 + _currentDigPin, _DigLevel);
    }
}

void SevenSegDisp::brightnessHandler(void) {
    digitalWrite(_digPin1 + _currentDigPin, !_DigLevel);
}

void SevenSegDisp::adjBrightness(void) {
    _brightness = 100 - (analogRead(_lightSensorPin) / 10);
    if (_brightness != _brightnessOld) {
        if (_brightnessDelay >= BRIGHT_DELAY) {
            if (_brightness > _brightnessOld)
                _brightnessOld += 1;
            else if (_brightness < _brightnessOld) {
                _brightnessOld -= 1;
            }
            if (_brightnessOld < BRIGHTNESS_MIN) {
                _brightnessOld = BRIGHTNESS_MIN;
            }
        } else {
            _brightnessDelay++;
        }
    } else {
        _brightnessDelay = 0;
    }

    if (OCR0A != _brightnessOld) {
        if (_timerID == 0) {
            OCR0A = (float) 255 / 100 * _brightnessOld;
        } else if (_timerID == 1) {
            OCR1A = (float) 255 / 100 * _brightnessOld;
        } else if (_timerID == 2) {
            OCR2A = (float) 255 / 100 * _brightnessOld;
        }
    }
}

void SevenSegDisp::write(int16_t value) {
    _dispValue = value;
}
