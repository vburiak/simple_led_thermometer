# Simple LED thermometer

#### Description
Thermometer based on DS18B20 sensor and 7 segment 2 digits LED display.
*The thermometer also contains light sensor and display brightness auto adjustment.
7 segment display can be small which does not require additional transistor or it can be big LED display with
additional transistors for common cathode or anode, this can be specified in source before compilation.*

#### Elements
* arduino board (nano 1pc.)
* 7 segments LED display (SH18101AS RED 2pc.)
* transisstors for common cathode switch (S8050 2pc.)
* themperature sensor (DS18B20 1pc.)
* photoresistor (GL5516 1pc.)
* LED for minus sign when temperature is below -9 (5mm RED 2pc.)
* resistor for VT switch (10k 2pc.)
* resistor for VT switch (1k 2pc.)
* resistor for light sensor (10k 1pc.)
* resistor for temperature sensor pullup (4k7 1pc.)
